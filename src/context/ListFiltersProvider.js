import React from 'react';

const ListFilterContext = React.createContext();
const ListFilterConsumer = ListFilterContext.Consumer;

const withListFilter = (WrappedComponent) => {
	class HOC extends React.Component {
		render() {
			return <ListFilterConsumer>
				{({tenant}) => (
				  <WrappedComponent {...this.props} tenant={tenant}/>
                )}
            </ListFilterConsumer>
		}
	}

	return HOC;
};

class ListFiltersProvider extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            tenant: props.user.data.default_tenant,
            tenants: props.user.data.tenants,
        };
    }

    setTenant = (tenantId) => {
        this.setState({tenant: tenantId}, () => {
        });
    };

    render () {
        const { children } = this.props;
        return (
            <ListFilterContext.Provider value={{setTenant: this.setTenant, ...this.state}}>
                {children}
            </ListFilterContext.Provider>
        )
    }
}

export {
    ListFiltersProvider,
    ListFilterConsumer,
	withListFilter
};