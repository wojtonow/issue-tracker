import React from "react";
import PropTypes from 'prop-types';
import MUIDataTable from "mui-datatables";
import IssueTableFooter from './IssueTableFooter'

class IssueTable extends React.Component {
    
	customFooter = (count, page, rowsPerPage, changeRowsPerPage, changePage) => {
		return <IssueTableFooter changePage={changePage} count={count} page={page} rowsPerPage={rowsPerPage} changeRowsPerPage={changeRowsPerPage} rowsPerPageOptions={this.props.options.rowsPerPageOptions}/>;
	};

	render(){
		const { options, ...rest } = this.props;
		options.customFooter = this.customFooter;
		
		return(
		  <MUIDataTable options={options} {...rest}/>
		)
	}
}


IssueTable.propTypes = {
    classes: PropTypes.object,
    options: PropTypes.object.isRequired
};

export default IssueTable;