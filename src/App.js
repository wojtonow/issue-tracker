import React from 'react';
import { createMuiTheme } from '@material-ui/core/styles';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import blue from '@material-ui/core/colors/blue';

import Navbar from './components/Nabar';
import IssueList from './components/IssueList/IssueList';

const theme = createMuiTheme({
  palette: {
      primary: {
        main: blue[500]
      }
  },
  typography: {
      useNextVariants: true,
  },
});

class App extends React.Component {

  render() {
      return (
          <MuiThemeProvider theme={theme}>
            <Navbar/>
            <IssueList />
          </MuiThemeProvider>
      );
  }
}

export default (App);
