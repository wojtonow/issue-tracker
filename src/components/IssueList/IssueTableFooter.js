import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';

const actionsStyles = theme => ({
	root: {
		flexShrink: 0,
		color: theme.palette.text.secondary,
		marginLeft: theme.spacing.unit * 2.5,
	},
});

class TablePaginationActions extends React.Component {
	handleFirstPageButtonClick = event => {
		this.props.onChangePage(event, 0);
	};
	
	handleBackButtonClick = event => {
		this.props.onChangePage(event, this.props.page - 1);
	};
	
	handleNextButtonClick = event => {
		this.props.onChangePage(event, this.props.page + 1);
	};
	
	handleLastPageButtonClick = event => {
		this.props.onChangePage(
			event,
			Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1),
		);
	};
	
	render() {
		const { classes, count, page, rowsPerPage, theme } = this.props;
		
		return (
			<div className={classes.root}>
				<Tooltip title="First Page" >
					<IconButton
						onClick={this.handleFirstPageButtonClick}
						disabled={page === 0}
						aria-label="First Page"
					>
						{theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
					</IconButton>
				</Tooltip>
				<Tooltip title="Previous Page" >
					<IconButton
						onClick={this.handleBackButtonClick}
						disabled={page === 0}
						aria-label="Previous Page"
					>
						{theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
					</IconButton>
				</Tooltip>
				<Tooltip title="Next Page" >
					<IconButton
						onClick={this.handleNextButtonClick}
						disabled={page >= Math.ceil(count / rowsPerPage) - 1}
						aria-label="Next Page"
					>
						{theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
					</IconButton>
				</Tooltip>
				<Tooltip title="Last Page" >
					<IconButton
						onClick={this.handleLastPageButtonClick}
						disabled={page >= Math.ceil(count / rowsPerPage) - 1}
						aria-label="Last Page"
					>
						{theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
					</IconButton>
				</Tooltip>
			</div>
		);
	}
}

TablePaginationActions.propTypes = {
	classes: PropTypes.object.isRequired,
	count: PropTypes.number.isRequired,
	onChangePage: PropTypes.func.isRequired,
	page: PropTypes.number.isRequired,
	rowsPerPage: PropTypes.number.isRequired,
	theme: PropTypes.object.isRequired,
};

const TablePaginationActionsWrapped = withStyles(actionsStyles, { withTheme: true })(
	TablePaginationActions,
);

class IssueTableFooter extends React.Component {
	
	handleChangePage = (event, page) => {
		if(typeof this.props.changePage ==='function'){
			this.props.changePage(page);
		}
	};
	
	handleChangeRowsPerPage = event => {
		if(typeof this.props.changeRowsPerPage ==='function'){
			this.props.changeRowsPerPage(event.target.value);
		}
	};
	
	render() {
		const {count, page, rowsPerPage, rowsPerPageOptions} = this.props;
		return (
			<TableFooter>
				<TableRow>
					<TablePagination
						rowsPerPageOptions={rowsPerPageOptions}
						count={count}
						rowsPerPage={rowsPerPage}
						page={page}
						onChangePage={this.handleChangePage}
						onChangeRowsPerPage={this.handleChangeRowsPerPage}
						ActionsComponent={TablePaginationActionsWrapped}
					/>
				</TableRow>
			</TableFooter>
		)
	}
}

IssueTableFooter.propTypes = {
	classes: PropTypes.object,
	count: PropTypes.number.isRequired,
	page: PropTypes.number.isRequired,
	rowsPerPage: PropTypes.number.isRequired,
	changePage: PropTypes.func.isRequired,
	changeRowsPerPage: PropTypes.func.isRequired,
	rowsPerPageOptions: PropTypes.array
};

export default IssueTableFooter;