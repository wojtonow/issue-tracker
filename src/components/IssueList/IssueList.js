import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Button, Dialog, Slide } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import IssueTable from "./IssueTable";
import moment from 'moment';

import mock from '../../mock_data';
import IssueDetail from '../IssueDetail/IssueDetail';

const styles = theme => ({
    leftColumn:{
        paddingLeft: theme.spacing.unit*3,
        paddingRight: theme.spacing.unit*3
    },
    stateFilter: {
        fontSize: `${0.7}em`
    },
    rightIcon: {
        marginLeft: theme.spacing.unit,
    },
});

const columns = [
    {
        name: "id",
        label: "ID",
        options: {
            filter: false,
            sort: true,
        }
    }, {
        name: "title",
        label: "Title",
        options: {
            filter: false,
            sort: true,
        }
    }, {
        name: "reporter",
        label: "Reporter",
        options: {
            filter: false,
            sort: true,
        }
    },{
        name: "updated",
        label: "Last update",
        options: {
            filter: false,
            sort: true,
        }
    },{
        name: "created",
        label: "Created on",
        options: {
            filter: false,
            sort: true,
        }
    },{
        name: "asigned",
        label: "Asigned to",
        options: {
            filter: false,
            sort: true,
        }
    },{
        name: "priority",
        label: "Priority",
        options: {
            filter: true,
            sort: true,
            filterType: 'multiselect',
        }
    },{
        name: "status",
        label: "Staus",
        options: {
            filter: true,
            sort: true,
            filterType: 'multiselect',
        }
    }
];

function Transition(props) {
    return <Slide direction="up" {...props} />;
  }

class IssueList extends React.Component{
    state = {
        data: mock,
        open: false,
        issue: {}
    };

    handleCreateIssue = () => {
        this.setState({open: true});
    }

    handleClose = issue => {
        if(issue){
            // new issue
            let clone = this.state.data.splice(0), updated;
            if(issue.created===null){
                issue.created = moment().format("YYYY-MM-DD HH:mm:ss ");
                clone.push(issue);
                updated = clone;
            }else{
                issue.updated = moment().format("YYYY-MM-DD HH:mm:ss ");
                updated = clone.map(row => {
                    if(row.id===issue.id){
                        Object.assign(row, issue);
                    }
                    return row;
                });
            }
            
            this.setState({open: false, data: updated, issue: {}});
        }else{
            this.setState({open: false, issue: {}})
        }
    }

    handleEdit = (rowData, rowMeta, index) =>{
        this.setState({issue: this.state.data[rowMeta.dataIndex], open: true})
    }


    render() {
        const {classes} = this.props;

        return (
            <React.Fragment>
                <IssueTable
                    title=""
                    data={this.state.data}
                    columns={columns}
                    options={{
                        filter: true,
                        search: true,
                        print: false,
                        download: false,
                        selectableRows: false,
                        viewColumns: false,
                        fixedHeader: true,
                        onRowClick: this.handleEdit,
                        customToolbar: () =>(
                            <Button variant="contained" color="secondary" onClick={this.handleCreateIssue} >
                                Create Issue
                                <AddIcon className={classes.rightIcon} />
                            </Button>
                          )
                    }}
                />
                <Dialog
                    fullScreen
                    open={this.state.open}
                    TransitionComponent={Transition}
                    >
                    <IssueDetail issue={this.state.issue} onClose={this.handleClose}/>
                    </Dialog>
            </React.Fragment>
        );
    }
}


IssueList.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(IssueList);