import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {AppBar, Toolbar, Button, IconButton, Typography, TextField, InputLabel, MenuItem, FormControl, Select} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import ReactSelect from 'react-select';


import mock from '../../mock_data';
import { Grid } from '@material-ui/core';

const styles = theme => ({
    appBar: {
        position: 'relative',
      },
      flex: {
        flex: 1,
      },
      wrapper:{
          padding: theme.spacing.unit*4
      },
      reactSelect:{
          marginTop: theme.spacing.unit*2
      }
});

const defaultState = {
    id: null,
    title: '',
    description: '',
    status: 'open',
    priority: 'low',
    reporter: '',
    asigned: null,
    created: null,
    updated: null
}

const statuses = {
    open: ['open','pending', 'closed'],
    pending: ['pending', 'closed'],
    closed: ['closed'],
    new: ['open']
};

function capitalize(string) 
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}

class IssueDetail extends React.Component{

    constructor(props){
        super(props);

        this.state = {};
        this.newIssueId = 0; /* only for this case - new id shoulde be generated in database on save */
        this.userList = []; /* should be get from database */
        this.userListSuggestions = [];
        this.parseData(); /* only for mock data */
        Object.assign(this.state, defaultState, {id: this.newIssueId}, props.issue );

        if(props.issue.id && props.issue.id !==null){
            this.availableStatuses = this.getAvailableStatuses();
        }else{
            this.availableStatuses = statuses.new;
        }
        
    }

    parseData(){
        mock.forEach((item, index, arr) =>{
            this.newIssueId = Math.max(this.newIssueId, item.id);
            if(this.userList.indexOf(item.reporter)===-1){
                this.userList.push(item.reporter);
                this.userListSuggestions.push({value: item.reporter, label: item.reporter});
            }
            if(this.userList.indexOf(item.asigned)===-1){
                this.userList.push(item.asigned);
                this.userListSuggestions.push({value: item.asigned, label: item.asigned});
            }
        });
        this.newIssueId++;
    }


    handleChange = name => event => {
        this.setState({ [name]: event.target.value });
    };

    handleChangeAsigned = (selected) => {
        this.setState({ asigned: (selected!==null) ? selected.value : ''});
      }

    getAvailableStatuses = () => {
        return statuses.hasOwnProperty(this.state.status) ? statuses[this.state.status] : statuses['all'];
    }

    handleSave = () => {
        console.log('save')
        if(this.state.title && this.state.description){
            console.log('can save');
            this.props.onClose(this.state);
        }
    }

    render() {
        const {classes} = this.props;
        const DialogTitle = this.state.created === null ? 'Create Issue' : 'Edit Issue';
        return (
            <React.Fragment>
                <AppBar className={classes.appBar}>
                    <Toolbar>
                    <IconButton color="inherit" onClick={ev => this.props.onClose()} aria-label="Close">
                        <CloseIcon />
                    </IconButton>
                    <Typography variant="h6" color="inherit" className={classes.flex}>
                       {DialogTitle}
                    </Typography>
                    <Button color="inherit" onClick={this.handleSave}>
                        SAVE
                    </Button>
                    </Toolbar>
                </AppBar>
                <form className={classes.wrapper} autoComplete="off">
                    <Grid container>
                        <Grid item sm={3}>
                            <TextField
                                id="issue-id"
                                label="Issue id"
                                className={classes.textField}
                                value={this.state.id}
                                margin="normal"
                                disabled
                            />
                        </Grid>
                        <Grid item sm={3}>
                        <FormControl className={classes.formControl}>
                            <InputLabel htmlFor="status">State</InputLabel>
                            <Select
                                value={this.state.status}
                                onChange={this.handleChange('status')}
                                inputProps={{
                                name: 'status',
                                id: 'status',
                                }}
                            >
                                {this.availableStatuses.map(item =>(<MenuItem key={item} value={item}>{capitalize(item)}</MenuItem>))}
                            </Select>
                        </FormControl>
                        </Grid>
                        <Grid item sm={3}>
                            <FormControl className={classes.formControl}>
                                <InputLabel htmlFor="priority">Priority</InputLabel>
                                <Select
                                    value={this.state.priority}
                                    onChange={this.handleChange('priority')}
                                    inputProps={{
                                    name: 'priority',
                                    id: 'priority',
                                    }}
                                >
                                    <MenuItem value="low">Low</MenuItem>
                                    <MenuItem value="medium">Medium</MenuItem>
                                    <MenuItem value="high">High</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item sm={3}>
                            <FormControl className={classes.formControl} fullWidth>
                            <InputLabel htmlFor="asigned" shrink={true}>Asigned to</InputLabel>
                            <ReactSelect
                                className={classes.reactSelect}
                                value={{value: this.state.asigned, label: this.state.asigned}}
                                onChange={this.handleChangeAsigned}
                                options={this.userListSuggestions}
                                inputId='asigned'
                            />
                            </FormControl>
                        </Grid>
                    </Grid>
                    <TextField
                        id="issue-title"
                        label="Title"
                        className={classes.textField}
                        value={this.state.title}
                        onChange={this.handleChange('title')}
                        margin="normal"
                        fullWidth
                        required
                    />
                <TextField
                        id="issue-description"
                        label="Description"
                        className={classes.textField}
                        value={this.state.description}
                        onChange={this.handleChange('description')}
                        margin="normal"
                        fullWidth
                        multiline={true}
                        required
                    />
                </form>
            </React.Fragment>
            
        );
    }
}


IssueDetail.propTypes = {
    classes: PropTypes.object.isRequired,
    onClose: PropTypes.func.isRequired,
    issue: PropTypes.object.isRequired
};

export default withStyles(styles)(IssueDetail);